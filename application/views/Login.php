<div class="container">
	<div class="row">
		<div class="col-md-12">
      		<div class="card" style="padding: 10px;">
        		<form action="<?= base_url('Customer/aksi_login'); ?>" method="post">
				  <div class="form-group">
				    <label for="username"> Email or Username </label>
					<input id="username" class="form-control" name="username" required="required" type="text"/>
				  </div>
				  <div class="form-group">
				    <label for="password" class="password"> Password </label>
					<input id="password" class="form-control" name="password" required="required" type="password"/>
				  </div>
				  <button type="submit" name="simpan" class="btn btn-primary">Submit</button><br>
				  Not a member yet ?
				  <a href="<?= base_url('Customer/registerView'); ?>" class="register">Join us</a>
				</form>
    		</div>
		</div>
	</div>
</div>