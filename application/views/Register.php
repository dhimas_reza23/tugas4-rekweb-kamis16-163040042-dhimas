<title>Register</title>
<div class="container">
	<div class="row">
		<div class="col-md-12">
      		<div class="card" style="padding: 10px;">
        		<form  action="<?= base_url('Customer/simpanCus'); ?>" method="post"> 
				<h1> Sign up </h1>
				<div class="form-group">
					<label for="nama">Username</label>
					<input id="nama" name="nama" class="form-control" required="required" type="text"/>
				</div>
				<div class="form-group">
					<label for="email"> Email</label>
					<input id="email" name="email" class="form-control" required="required" type="email"/> 
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input id="password" class="form-control" name="password" required="required" type="password"/>
				</div>
				<div class="form-group">
					<label for="alamat">Alamat</label>
					<input type="text" class="form-control" name="alamat" id="alamat " required="required">
				</div>
				<div class="form-group">
					<label for="jk">Jenis Kelamin</label>
					<input type="radio"  name="jk" id="jk" value="Laki-Laki"> Laki-Laki <input type="radio"  name="jk" id="jk" value="perempuan"> Perempuan
				</div>
				<div class="form-group">
					<label for="tgl_lahir">Tanggal Lahir</label>
					<input type="date" class="form-control" name="tgl_lahir" id="tgl_lahir" required="required">
				</div>
				<div class="form-group">
					<label for="notel">No Telepon</label>
					<input type="text" class="form-control" name="notel" id="notel" required="required">
				</div>
				<div class="form-group">
					<button type="submit" name="submit" class="btn btn-primary">Register</button>
				</div>
			</form>
    		</div>
		</div>
	</div>
</div>