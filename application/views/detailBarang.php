
   <!-- HEADER -->
    <header>
      <!-- TOP HEADER -->
      <div id="top-header">
        <div class="container">
          <ul class="header-links pull-left">
            <li><a href="#"><i class="fa fa-phone"></i> +021-95-51-84</a></li>
            <li><a href="#"><i class="fa fa-envelope-o"></i> B4.pharmacy@Gmail.com</a></li>
            <li><a href="#"><i class="fa fa-map-marker"></i> Jl. DR. Setiabudhi No.193</a></li>
          </ul>
          <ul class="header-links pull-right">
            <li><a href="<?= base_url("Todo/showCart"); ?>"><i class="fa fa-dollar"></i> Shopping Cart <?= $total_cart; ?></a></li>
            <?php if ($this->session->userdata('status') == 'login'): ?>
                    <li><a href="#"><i class="fa fa-user-o"></i> <?= $this->session->userdata('nama'); ?> </a></li>
            <?php else : ?>
                    <li><a href="#"><i class="fa fa-user-o"></i>Account </a></li>
            <?php endif; ?>
          </ul>
        </div>
      </div>
      <!-- /TOP HEADER -->

      <!-- MAIN HEADER -->
      <div id="header">
        <!-- container -->
        <div class="container">
          <!-- row -->
          <div class="row">
            <!-- LOGO -->
            <div class="col-md-3">
              <div class="header-logo">
                <a href="#" class="logo">
                  <img src="/assets/images/B4.jpg" alt="">
                </a>
              </div>
            </div>
            <!-- /LOGO -->

            <!-- SEARCH BAR -->
            <div class="col-md-6">
              <div class="header-search">
                <form action="<?= base_url("Todo/cari"); ?>">
                  <input class="input" placeholder="Search here" name="cari">
                  <button class="search-btn">Search</button>
                </form>
              </div>
            </div>
            <!-- /SEARCH BAR -->
          </div>
          <!-- row -->
        </div>
        <!-- container -->
      </div>
      <!-- /MAIN HEADER -->
    </header>
    <!-- /HEADER -->

 <!-- BREADCRUMB -->
    <div id="breadcrumb" class="section">
      <!-- container -->
      <div class="container">
        <!-- row -->
        <div class="row">
          <div class="col-md-12">
            <ul class="breadcrumb-tree">
              <li><a href="#">Home</a></li>
              <li><a href="#">All Categories</a></li>
            </ul>
          </div>
        </div>
        <!-- /row -->
      </div>
      <!-- /container -->
    </div>
    <!-- /BREADCRUMB -->

    <!-- SECTION -->
    <div class="section">
      <!-- container -->
      <div class="container">
        <h1>Detail Obat</h1>
        <br>
        <!-- row -->
        <div class="row">
          <!-- Product main img -->
          <div class="col-md-5 col-md-push-2">
            <div id="product-main-img">
              <div class="product-preview">
                <img src="<?= base_url('assets/images/'. $detail->gambar); ?>" alt="">
              </div>
            </div>
          </div>
          <!-- /Product main img -->

          <!-- Product details -->
          <div class="col-md-5">
            <div class="product-details">
              <h2 class="product-name"><?= $detail->nama_obat; ?></h2>
              <div>
                <div class="product-rating">
                </div>
                <a class="review-link" href="#">10 Review(s) | Add your review</a>
              </div>
              <div>
                <h3 class="product-price">Rp. <?= $detail->harga; ?></h3>
              </div>
              <p><?= $detail->keterangan; ?></p>

              <div class="product-options">
                 
              </div>

              <div class="add-to-cart">
                <div class="qty-label">
                </div>
                <a href="<?= base_url('Todo/addCart/'.$detail->id); ?>" class="btn btn-info">
                <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button></a>
              </div>

              <ul class="product-btns">
              </ul>

              <ul class="product-links">
                <li>Category:</li>
              </ul>

              <ul class="product-links">
                <li>Share:</li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-envelope"></i></a></li>
              </ul>

            </div>
          </div>
          <!-- /Product details -->
        </div>
        <!-- /row -->
      </div>
      <!-- /container -->
    </div>
     <br>
          <br>
          <br>
    <!-- /SECTION -->

<!-- FOOTER -->
    <footer id="footer">
      <!-- top footer -->
      <div class="section">
        <!-- container -->
        <div class="container">
          <!-- row -->
          <div class="row">
            <div class="col-md-3 col-xs-6">
              <div class="footer">
                <h3 class="footer-title">About Us</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
                <ul class="footer-links">
                  <li><a href="#"><i class="fa fa-map-marker"></i>Jl. DR. Setiabudhi No.193</a></li>
                  <li><a href="#"><i class="fa fa-phone"></i>+021-95-51-84</a></li>
                  <li><a href="#"><i class="fa fa-envelope-o"></i>B4.pharmacy@Gmail.com</a></li>
                </ul>
              </div>
            </div>

            <div class="clearfix visible-xs"></div>

            <div class="col-md-3 col-xs-6">
              <div class="footer">
                <h3 class="footer-title">Information</h3>
                <ul class="footer-links">
                  <li><a href="#">About Us</a></li>
                  <li><a href="#">Contact Us</a></li>
                  <li><a href="#">Privacy Policy</a></li>
                  <li><a href="#">Orders and Returns</a></li>
                  <li><a href="#">Terms & Conditions</a></li>
                </ul>
              </div>
            </div>

            <div class="col-md-3 col-xs-6">
              <div class="footer">
                <h3 class="footer-title">Service</h3>
                <ul class="footer-links">
                  <li><a href="#">My Account</a></li>
                  <li><a href="#">View Cart</a></li>
                  <li><a href="#">Wishlist</a></li>
                  <li><a href="#">Track My Order</a></li>
                  <li><a href="#">Help</a></li>
                </ul>
              </div>
            </div>
          </div>
          <!-- /row -->
        </div>
        <!-- /container -->
      </div>
      <!-- /top footer -->

      <!-- bottom footer -->
      <div id="bottom-footer" class="section">
        <div class="container">
          <!-- row -->
          <div class="row">
            <div class="col-md-12 text-center">
              <ul class="footer-payments">
                <li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
                <li><a href="#"><i class="fa fa-credit-card"></i></a></li>
                <li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
                <li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
                <li><a href="#"><i class="fa fa-cc-discover"></i></a></li>
                <li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
              </ul>
              
            </div>
          </div>
            <!-- /row -->
        </div>
        <!-- /container -->
      </div>
      <!-- /bottom footer -->
    </footer>
    <!-- /FOOTER -->