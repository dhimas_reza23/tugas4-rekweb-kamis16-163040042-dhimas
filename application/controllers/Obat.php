<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// ketikan source yang ada di modul
		$this->API = "http://localhost/rekweb/praktikum/tugas4/rest_server_obat";
	}

	public function index()
	{
		$data['obat'] = json_decode($this->curl->simple_get($this->API .'/obat/'));
		$data['jenis'] = json_decode($this->curl->simple_get($this->API .'/jenis/'));
		$i = 0;
		foreach ($this->cart->contents() as $key) {
			$i++;

		}
		$data["total_cart"] = $i;
		$data['content'] = 'index';
		$this->load->view('templates/template', $data);

	}

	public function getObat(){
		$data['obat'] = json_decode($this->curl->simple_get($this->API .'/obat/'));
		$this->load->view('admin/Obat', $data);
	}

	public function getForm(){
		$data['jenis'] = json_decode($this->curl->simple_get($this->API .'/jenis/'));
		$this->load->view('admin/Forms_tambah_Obat', $data);
	}

	public function detailBarang($id){
		$data['detail'] = json_decode($this->curl->simple_get($this->API .'/obat?id='. $id));
		$i = 0;
		foreach ($this->cart->contents() as $key) {
			$i++;
		}
		$data["total_cart"] = $i;
		$data['content'] = 'detailBarang';
		$this->load->view('templates/template', $data);
	}

	public function addCart($id){
		$getData = json_decode($this->curl->simple_get($this->API .'/obat?id='. $id));
		$data = array('id'=>$getData[0]->id, 
					'name'=>$getData[0]->nama_obat,
					'qty' => 1, 
					'price'=>$getData[0]->harga);
		$i = 0;
		foreach ($this->cart->contents() as $key) {

			if ($getData[0]->stok_obat <= $key['qty']) {
				$this->session->set_flashdata('status_obat', '<h3 style="color: red;">Stok Obat Kurang</h3>');
				redirect('Obat/showCart');
			}
			$i++;
		}
		$this->cart->insert($data);
		$halaman['total_cart'] = $i;
		$halaman['content'] = 'Keranjang_Belanja';
		$this->load->view('templates/template', $halaman);
	}

	public function hapusCart($rowid){
		$this->cart->update(array('rowid' => $rowid, 'qty'=>0));
		$i = 0;
		foreach ($this->cart->contents() as $key) {
			$i++;
		}
		$data["total_cart"] = $i;
		$data['content'] = 'Keranjang_Belanja';
		$this->load->view('templates/template', $data);
	}

	public function updateCart(){
		$i = 1;
		$u = 0;
		foreach ($this->cart->contents() as $obat) {
			$getData = json_decode($this->curl->simple_get($this->API .'/obat?id='. $obat['id']));
			if ($getData[0]->stok_obat < $_POST[$i.'qty']) {
				$this->session->set_flashdata('status_obat', '<h3 style="color: red;">Stok Obat Kurang</h3>');
				redirect('Obat/showCart');
			}
			else{
				$this->cart->update(array('rowid'=>$obat['rowid'], 'qty'=>$_POST[$i.'qty']));
				$i++;
				$u++;
			}
		}
		$data["total_cart"] = $u;
		$data['content'] = 'Keranjang_Belanja';
		$this->load->view('templates/template', $data);
	}

	public function showCart(){
		$keranjang = $this->cart->contents();
		$i = 0;
		foreach ($this->cart->contents() as $key) {
			$i++;
		}
		$data["total_cart"] = $i;
		$data['content'] = 'Keranjang_Belanja';
		$this->load->view('templates/template', $data);
	}

	public function cari(){
		$cari = $this->input->get('cari');
		$data['obat'] = json_decode($this->curl->simple_get($this->API .'/obat?cari='. $cari));
		$data['jenis'] = json_decode($this->curl->simple_get($this->API .'/jenis/'));
		$i = 0;
		foreach ($this->cart->contents() as $key) {
			$i++;
		}
		$data["total_cart"] = $i;
		$data['content'] = 'index';

		$this->load->view('templates/template', $data);
	}

	public function cariHalAdmin(){
		$cari = $this->input->get('cari');
		$data['obat'] = json_decode($this->curl->simple_get($this->API .'/obat?cari='. $cari));
		$data['jenis'] = json_decode($this->curl->simple_get($this->API .'/jenis/'));
		$i = 0;

		$this->load->view('admin/Obat', $data);
	}

	public function insert()
	{
		// ketikan source code yang ada di modul
		$data['content'] = 'insert';
		$this->load->view('templates/template', $data);
	}

	public function tambah() 
	{
		// ketikan source code yang ada di modul
		$obat['nama_obat'] = $this->input->post('nama_obat');
		$obat['id_jenis'] = $this->input->post('jenis_obat');
		$obat['stok_obat'] = $this->input->post('stok_obat');
		$obat['tgl_exp'] = $this->input->post('tgl');
		$obat['harga'] = $this->input->post('harga');
		$obat['keterangan'] = $this->input->post('keterangan');
		$config['upload_path']          = "./assets/images";
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1000;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('gambar'))
        {
                $error = array('error' => $this->upload->display_errors());

                $this->load->view('admin/Forms_tambah_Obat', $error);
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                $obat['gambar'] = $this->upload->data('file_name');

				$this->curl->simple_post($this->API .'/obat/', $obat, array(CURLOPT_BUFFERSIZE => 10));
				if ($status) {
					$this->session->set_flashdata('status', '<h3 style="color: blue;">Obat Berhasil Ditambahkan</h3>');
				}
				else{
					$this->session->set_flashdata('status', '<h3 style="color: red;">Obat Gagal Ditambahkan</h3>');
				}

				redirect('Obat/getObat');
        }
			
	}

	public function eObat($id){
		$data['obat'] = json_decode($this->curl->simple_get($this->API .'/obat?id='. $id));
		$data['jenis'] = json_decode($this->curl->simple_get($this->API .'/jenis/'));
		$this->load->view('admin/ubahobat', $data);
	}

	public function edit(){
		$obat['id'] = $this->input->post('id');
		$obat['nama_obat'] = $this->input->post('nama_obat');
		$obat['id_jenis'] = $this->input->post('jenis_obat');
		$obat['stok_obat'] = $this->input->post('stok_obat');
		$obat['tgl_exp'] = $this->input->post('tgl');
		$obat['harga'] = $this->input->post('harga');
		$obat['keterangan'] = $this->input->post('keterangan');
		$config['upload_path']          = "./assets/images";
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1000;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('gambar'))
        {
                $error = array('error' => $this->upload->display_errors());

                $this->load->view('admin/Forms_tambah_Obat', $error);
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                $obat['gambar'] = $this->upload->data('file_name');

				$this->curl->simple_put($this->API .'/obat/', $obat, array(CURLOPT_BUFFERSIZE => 10));
				if ($status) {
					$this->session->set_flashdata('status', '<h3 style="color: blue;">Obat Berhasil Ditambahkan</h3>');
				}
				else{
					$this->session->set_flashdata('status', '<h3 style="color: red;">Obat Gagal Ditambahkan</h3>');
				}

				redirect('Obat/getObat');
        }
	}

	// public function selesai($Obat){
	// 	$this->_rules();
	// 	$data['id_Obat'] = $Obat;
	// 	$data['status'] = 1;
	// 	if ($status) {
	// 			$this->session->set_flashdata('status', '<h3 style="color: blue;">Obat Berhasil Diubah</h3>');
	// 	}
	// 		else{
	// 			$this->session->set_flashdata('status', '<h3 style="color: red;">Obat Gagal Diubah</h3>');
	// 	}
	// 	redirect('Obat');
	// }

	// public function undo($Obat){
	// 	$this->_rules();
	// 	$this->load->model('Obat_model');
	// 	$data['id_Obat'] = $Obat;
	// 	$data['status'] = 0;
	// 	$status = $this->Obat_model->updateObat($data);
	// 	if ($status) {
	// 			$this->session->set_flashdata('status', '<h3 style="color: blue;">Obat Berhasil Diubah</h3>');
	// 	}
	// 		else{
	// 			$this->session->set_flashdata('status', '<h3 style="color: red;">Obat Gagal Diubah</h3>');
	// 	}
	// 	redirect('Obat');
	// }

	public function hapus($id){
		$this->_rules();
		$this->curl->simple_delete($this->API .'/obat/', array('id'=>$id), array(CURLOPT_BUFFERSIZE => 10));
		if ($status) {
				$this->session->set_flashdata('status', '<h3 style="color: blue;"> Berhasil Dihapus</h3>');
		}
			else{
				$this->session->set_flashdata('status', '<h3 style="color: red;"> Gagal Dihapus</h3>');
		}
		redirect('Obat/getObat');
	}

	public function _rules()
	{
		// ketikan source code yang ada di modul
		$this->form_validation->set_rules('Obat','nama Obatlist isi','trim|required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">','</span>');	
	}
}
