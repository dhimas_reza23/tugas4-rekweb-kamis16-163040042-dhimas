<?php
/**
 * 
 */
class Jenis extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/rekweb/praktikum/tugas4/rest_server_obat";
	}

	public function index(){
		$data['jenis'] = json_decode($this->curl->simple_get($this->API .'/jenis/'));
		$this->load->view('admin/Jenis', $data);
	}

	public function TambahJenis(){

		$jenis['nama_jenis'] = $this->input->post('nama_jenis');
		$this->curl->simple_post($this->API .'/jenis/', $jenis, array(CURLOPT_BUFFERSIZE => 10));
		redirect('jenis');
	}

	public function hapusJenis($id_jenis){
		$this->curl->simple_delete($this->API .'/jenis/', array('id_jenis'=>$id_jenis), array(CURLOPT_BUFFERSIZE => 10));
		
		redirect('jenis');
	}

	public function eJenis($id_jenis){
		$data['jenis'] = json_decode($this->curl->simple_get($this->API .'/jenis?id='. $id_jenis));
		$this->load->view('admin/ubahjenis', $data);
	}

	public function editJenis(){
		$jenis['id_jenis'] = $this->input->post('id_jenis');
		$jenis['nama_jenis'] = $this->input->post('nama_jenis');
		$this->curl->simple_put($this->API .'/jenis/', $jenis, array(CURLOPT_BUFFERSIZE => 10));

		redirect('jenis');
	}

	public function cari(){
		$cari = $this->input->get('cari');
		$data['jenis'] = json_decode($this->curl->simple_get($this->API .'/jenis?cari='. $cari));
		$i = 0;

		$this->load->view('admin/Jenis', $data);
	}

}
?>