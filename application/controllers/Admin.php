<?php 
/**
 * 
 */
class Admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/rekweb/praktikum/tugas4/rest_server_obat";
		
	}
	
	public function index(){
		$this->load->view('admin/Login');
		
	}
	
	public function checkLogin(){
		
		redirect('Pesanan/index');
		
	}
	public function tambahJenis(){
		$this->load->view('admin/Forms_tambah_jenis_Obat');
	}
	public function tambahObat(){
		redirect('obat/getForm');
	}
	public function Pesanan(){
		redirect('Pesanan/index');
	}
	public function obat(){
		redirect('obat/getObat');
	}

	public function jenisObat(){
		redirect('jenis/index');
	}

	public function customer(){
		redirect('customer/getCus');
	}

	public function logout(){
			$this->session->sess_destroy();
			redirect('Admin');
	}

	public function aksi_login(){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$where = array(
					"username"=>$username,
					"password"=>$password);
			$cek = json_decode($this->curl->simple_get($this->API .'/admin?password='. $password));
			if ($cek > 0) {
				$data_session = array(
								"nama"=>$cek[0]->username,
								"status_admin"=>"login");
				$this->session->set_userdata($data_session);
				redirect('Pesanan/index');
			}
			else{
				redirect('pesanan');
			}
		}
}