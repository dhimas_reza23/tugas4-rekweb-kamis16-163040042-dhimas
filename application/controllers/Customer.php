<?php
/**
 * 
 */
class Customer extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/rekweb/praktikum/tugas4/rest_server_obat";
	}

	public function index(){
		$data['content'] = 'Login';
		$this->load->view('templates/template', $data);
	}

	public function getCus(){
		$data['cus'] = json_decode($this->curl->simple_get($this->API .'/customer/'));
		$this->load->view('admin/Customer', $data);
	}

	public function checkOut(){
		if ($this->session->userdata('status') != 'login') {
			redirect('Customer/login');
		}
		else{
			redirect('Pesanan/savePembelian');
		}
	}

	public function login(){
		$data['content'] = 'Login';
		$this->load->view('templates/template', $data);
	}

	public function registerView(){
		$data['content'] = 'Register';
		$this->load->view('templates/template', $data);
	}

	public function simpanCus(){
		$data = array(
				"nama"=>$this->input->post('nama'),
				"alamat"=>$this->input->post('alamat'),
				"email"=>$this->input->post('email'),
				"password"=>$this->input->post('password'),
				"jenis_kelamin"=>$this->input->post('jk'),
				"tgl_lahir"=>$this->input->post('tgl_lahir'),
				"no_telp"=>$this->input->post('notel'));

			$this->curl->simple_post($this->API .'/customer/', $data, array(CURLOPT_BUFFERSIZE => 10));
			redirect('Customer/login');
		}

		public function aksi_login(){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$where = array(
					"nama"=>$username,
					"email"=>$username,
					"password"=>$password);
			$cek = json_decode($this->curl->simple_get($this->API .'/customer?password='. $password));
			echo $cek[0]->id_customer;
			if ($cek > 0) {
				$data_session = array(
								"nama"=>$username,
								"status"=>"login",
								"id_customer"=>$cek[0]->id_customer);
				$this->session->set_userdata($data_session);
				redirect('Obat');
			}
			else{
			  	redirect('customer');
			 }
		}

		public function cari(){
			$cari = $this->input->get('cari');
			$data['cus'] = json_decode($this->curl->simple_get($this->API .'/customer?cari='. $cari));
			$i = 0;

			$this->load->view('admin/Customer', $data);
		}

		public function logout(){
			$this->session->sess_destroy();
			redirect('Obat');
		}	
}
?>