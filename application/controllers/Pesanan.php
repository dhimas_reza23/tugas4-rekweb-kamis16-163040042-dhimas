<?php
/**
 * 
 */
class Pesanan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/rekweb/praktikum/tugas4/rest_server_obat";
	}

	public function index(){
		$data['pesanan'] = json_decode($this->curl->simple_get($this->API .'/pembelian/'));
		$this->load->view('admin/Pesanan', $data);
	}

	public function ubahStatus($id){
		$data['id_pembelian'] = $id;
		$data['status'] = "selesai";
		$this->curl->simple_put($this->API .'/pembelian/', $data, array(CURLOPT_BUFFERSIZE => 10));
		redirect('Pesanan');
	}

	public function savePembelian(){
		$i = 1;
		$jumlah = 0;
		foreach ($this->cart->contents() as $key) {
			# code...
			$getData = json_decode($this->curl->simple_get($this->API .'/obat?id='. $key['id']));
			$jumlah = $getData[0]->stok_obat-$key['qty'];
			$data = array(
					"id_customer"=>$this->session->userdata("id_customer"),
					"id_obat"=>$key['id'],
					"tgl_pembelian"=>date('Y-m-d'),
					"quantity"=>$key['qty'],
					"total_bayar"=>$key['subtotal'],
					"status"=>"dipesan"
					);
			$obat = array(
					"stok_obat"=>$jumlah,
					"id"=>$data['id_obat'],
					"nama_obat"=>$getData[0]->nama_obat,
					"id_jenis"=>$getData[0]->id_jenis,
					"tgl_exp"=>$getData[0]->tgl_exp,
					"harga"=>$getData[0]->harga,
					"gambar"=>$getData[0]->gambar,
					"keterangan"=>$getData[0]->keterangan);
			$this->curl->simple_post($this->API .'/pembelian/', $data, array(CURLOPT_BUFFERSIZE => 10));
			$this->curl->simple_put($this->API .'/obat/', $obat, array(CURLOPT_BUFFERSIZE => 10));
			$i++;
		}
		$this->cart->destroy();
		$halaman['content'] = 'CheckOut';
		$this->load->view('templates/template', $halaman);
	}
}
?>